######### Rust build container ############
FROM ekidd/rust-musl-builder:latest as build-env

WORKDIR /program

COPY Cargo.lock .
COPY Cargo.toml .
COPY src src

RUN cargo build --release

######## Runtime container ###########
FROM alpine:3.12.0

WORKDIR /program

COPY assets ./assets
COPY templates ./templates

COPY --from=build-env /program/target/x86_64-unknown-linux-musl/release/status-monitor .

CMD ./status-monitor

