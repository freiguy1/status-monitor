pub fn url_with_protocol(url: &String) -> String {
    if url.starts_with("http") {
        url.clone()
    } else {
        format!("http://{}", url)
    }
}
