use serde::Serialize;
use sqlx::{sqlite::SqlitePool, FromRow};
use tokio::fs::OpenOptions;

const DATABASE_FILE_NAME: &'static str = "data/database.db";

// Important that existing ones do not change!
const MIGRATION_SCRIPTS: [&'static str; 4] = [
    "CREATE TABLE services (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        name TEXT NOT NULL UNIQUE,
        description TEXT NOT NULL,
        repository_url TEXT NOT NULL,
        service_type INTEGER NOT NULL
    );",
    "CREATE TABLE service_environments (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        name TEXT NOT NULL,
        ping_url TEXT NOT NULL,
        link_url TEXT NULL,
        status INTEGER NOT NULL,
        service_id INTEGER NOT NULL,
        FOREIGN KEY (service_id)
            REFERENCES services (id),
        UNIQUE(name, service_id) ON CONFLICT ABORT
        );",
    r#"insert into services (name, description, repository_url, service_type) values ("Ethan's blog", "A site about Ethan", "https://gitlab.com/freiguy1/freiguy1.gitlab.io", 1);"#,
    r#"insert into service_environments (name, ping_url, link_url, status, service_id) values ("prod", "https://ethanfrei.com", NULL, 1, 1);"#,
];

pub async fn init() -> Result<Repository, sqlx::Error> {
    // create db if not exists, in scope
    let _ = OpenOptions::new()
        .write(true)
        .create(true)
        .open(DATABASE_FILE_NAME)
        .await
        .expect("couldn't open/create file");

    let db_pool = SqlitePool::connect(&format!("sqlite:{}", DATABASE_FILE_NAME))
        .await
        .expect("unable to connect to database");

    // create migration table if not exist
    sqlx::query("CREATE TABLE IF NOT EXISTS migrations ( num_migrations INTEGER NOT NULL );")
        .execute(&db_pool)
        .await?;

    // Select number of migrations
    let migrations_result_set =
        sqlx::query_as::<_, (u32,)>("SELECT num_migrations FROM migrations;")
            .fetch_optional(&db_pool)
            .await?;
    let completed_migrations: u32 = if let Some(row) = migrations_result_set {
        row.0
    } else {
        sqlx::query("INSERT INTO migrations (num_migrations) values (0);")
            .execute(&db_pool)
            .await?;
        0
    };

    // execute remaining migrations
    for script in MIGRATION_SCRIPTS.iter().skip(completed_migrations as usize) {
        sqlx::query(script).execute(&db_pool).await?;
    }

    // update migrations table/row
    sqlx::query("UPDATE migrations SET num_migrations = ?")
        .bind(MIGRATION_SCRIPTS.len() as u32)
        .execute(&db_pool)
        .await?;

    Ok(Repository { db_pool })
}

#[derive(Clone)]
pub struct Repository {
    db_pool: SqlitePool,
}

impl Repository {
    pub async fn get_all_services(&self) -> Result<Vec<Service>, sqlx::Error> {
        let environment_rows = sqlx::query_as::<_, ServiceEnvironmentRow>(
            r#"
SELECT id, name, ping_url, link_url, status, service_id
FROM service_environments
            "#,
        )
        .fetch_all(&self.db_pool)
        .await?;

        let service_rows = sqlx::query_as::<_, ServiceRow>(
            r#"
SELECT id, name, description, repository_url, service_type
FROM services
            "#,
        )
        .fetch_all(&self.db_pool)
        .await?;

        Ok(service_rows
            .into_iter()
            .map(|row| Service {
                id: row.id,
                name: row.name,
                description: row.description,
                repository_url: row.repository_url,
                service_type: row.service_type.into(),
                environments: environment_rows
                    .iter()
                    .filter(|env| env.service_id == row.id)
                    .map(|env| ServiceEnvironment {
                        id: env.id,
                        name: env.name.clone(),
                        status: env.status == 1,
                        ping_url: env.ping_url.clone(),
                        link_url: env.link_url.clone(),
                        service_id: env.service_id,
                    })
                    .collect(),
            })
            .collect())
    }

    pub async fn get_service(&self, id: u32) -> Result<Option<Service>, sqlx::Error> {
        let environment_rows = sqlx::query_as::<_, ServiceEnvironmentRow>(
            r#"
SELECT id, name, ping_url, link_url, status, service_id
FROM service_environments
WHERE service_id = ?
            "#,
        )
        .bind(id)
        .fetch_all(&self.db_pool)
        .await?;

        let service_row = sqlx::query_as::<_, ServiceRow>(
            r#"
SELECT id, name, description, repository_url, service_type
FROM services
WHERE id = ?
            "#,
        )
        .bind(id)
        .fetch_optional(&self.db_pool)
        .await?;

        Ok(service_row.map(|row| Service {
            id: row.id,
            name: row.name,
            description: row.description,
            repository_url: row.repository_url,
            service_type: row.service_type.into(),
            environments: environment_rows
                .iter()
                .filter(|env| env.service_id == row.id)
                .map(|env| ServiceEnvironment {
                    id: env.id,
                    name: env.name.clone(),
                    status: env.status == 1,
                    ping_url: env.ping_url.clone(),
                    link_url: env.link_url.clone(),
                    service_id: env.service_id,
                })
                .collect(),
        }))
    }

    pub async fn create_service(
        &self,
        name: &String,
        description: &String,
        repository_url: &String,
        service_type: ServiceType,
    ) -> Result<Service, sqlx::Error> {
        let new_id = sqlx::query(
            r#"
INSERT INTO services (name, description, repository_url, service_type)
VALUES (?, ?, ?, ?)
            "#,
        )
        .bind(name)
        .bind(description)
        .bind(repository_url)
        .bind(service_type.to_u32())
        .execute(&self.db_pool)
        .await?
        .last_insert_rowid();

        Ok(Service {
            id: new_id as u32,
            name: name.clone(),
            description: description.clone(),
            repository_url: repository_url.clone(),
            service_type,
            environments: vec![],
        })
    }

    pub async fn delete_service(&self, id: u32) -> Result<(), sqlx::Error> {
        sqlx::query(
            r#"
DELETE FROM service_environments
WHERE service_id = ?
            "#,
        )
        .bind(id)
        .execute(&self.db_pool)
        .await?;

        sqlx::query(
            r#"
DELETE FROM services
WHERE id = ?
            "#,
        )
        .bind(id)
        .execute(&self.db_pool)
        .await?;

        Ok(())
    }

    pub async fn get_environments_by_service_id(
        &self,
        service_id: u32,
    ) -> Result<Vec<ServiceEnvironment>, sqlx::Error> {
        let rows = sqlx::query_as::<_, ServiceEnvironmentRow>(
            r#"
SELECT id, name, ping_url, link_url, status, service_id
FROM service_environments
WHERE service_id = ?
            "#,
        )
        .bind(service_id)
        .fetch_all(&self.db_pool)
        .await?;

        Ok(rows
            .iter()
            .map(|env| ServiceEnvironment {
                id: env.id,
                name: env.name.clone(),
                status: env.status == 1,
                ping_url: env.ping_url.clone(),
                link_url: env.link_url.clone(),
                service_id: env.service_id,
            })
            .collect())
    }

    pub async fn get_environment(
        &self,
        id: u32,
    ) -> Result<Option<ServiceEnvironment>, sqlx::Error> {
        let row = sqlx::query_as::<_, ServiceEnvironmentRow>(
            r#"
SELECT id, name, ping_url, link_url, status, service_id
FROM service_environments
WHERE id = ?
            "#,
        )
        .bind(id)
        .fetch_optional(&self.db_pool)
        .await?;

        Ok(row.map(|env| ServiceEnvironment {
            id: env.id,
            name: env.name.clone(),
            status: env.status == 1,
            ping_url: env.ping_url.clone(),
            link_url: env.link_url.clone(),
            service_id: env.service_id,
        }))
    }

    pub async fn update_env_status(&self, env_id: u32, status: bool) -> Result<(), sqlx::Error> {
        sqlx::query(
            r#"
UPDATE service_environments
SET status = ?
WHERE id = ?
            "#,
        )
        .bind(if status { 1u32 } else { 0u32 })
        .bind(env_id)
        .execute(&self.db_pool)
        .await?;
        Ok(())
    }

    pub async fn edit_service(
        &self,
        id: u32,
        name: &String,
        description: &String,
        repository_url: &String,
        service_type: ServiceType,
    ) -> Result<(), sqlx::Error> {
        sqlx::query(
            r#"
UPDATE services
SET name = ?,
    description = ?,
    repository_url = ?,
    service_type = ?
WHERE id = ?
            "#,
        )
        .bind(name)
        .bind(description)
        .bind(repository_url)
        .bind(service_type.to_u32())
        .bind(id)
        .execute(&self.db_pool)
        .await?;

        Ok(())
    }

    pub async fn create_environment(
        &self,
        service_id: u32,
        name: &String,
        ping_url: &String,
        link_url: &Option<String>,
    ) -> Result<ServiceEnvironment, sqlx::Error> {
        let new_id = sqlx::query(
            r#"
INSERT INTO service_environments (name, ping_url, link_url, status, service_id)
VALUES (?, ?, ?, ?, ?)
            "#,
        )
        .bind(name)
        .bind(ping_url)
        .bind(link_url.as_ref())
        .bind(0u32)
        .bind(service_id)
        .execute(&self.db_pool)
        .await?
        .last_insert_rowid();

        Ok(ServiceEnvironment {
            id: new_id as u32,
            name: name.clone(),
            ping_url: ping_url.clone(),
            link_url: link_url.clone(),
            status: false,
            service_id,
        })
    }

    pub async fn delete_environment(&self, id: u32) -> Result<(), sqlx::Error> {
        sqlx::query(
            r#"
DELETE FROM service_environments
WHERE id = ?
            "#,
        )
        .bind(id)
        .execute(&self.db_pool)
        .await?;

        Ok(())
    }
}

//-------------------------
//         DAOS
//-------------------------
#[derive(FromRow)]
struct ServiceRow {
    id: u32,
    name: String,
    description: String,
    repository_url: String,
    service_type: u32,
}

#[derive(FromRow)]
pub struct ServiceEnvironmentRow {
    id: u32,
    name: String,
    ping_url: String,
    link_url: Option<String>,
    status: u32,
    service_id: u32,
}

//-------------------------
//       CONTRACTS
//-------------------------

#[derive(Serialize, Clone, PartialEq)]
pub enum ServiceType {
    WebApp,
    DomainService,
}

impl ServiceType {
    pub fn to_u32(&self) -> u32 {
        match &self {
            Self::WebApp => 1,
            Self::DomainService => 2,
        }
    }
}

impl From<u32> for ServiceType {
    fn from(val: u32) -> ServiceType {
        match val {
            1 => ServiceType::WebApp,
            2 => ServiceType::DomainService,
            _ => panic!("Unknown service type from u32. Must be bug!"),
        }
    }
}

#[derive(Serialize, Clone, PartialEq)]
pub struct ServiceEnvironment {
    pub id: u32,
    pub name: String,
    pub ping_url: String,
    pub link_url: Option<String>,
    pub status: bool,
    pub service_id: u32,
}

#[derive(Serialize, FromRow, Clone, PartialEq)]
pub struct Service {
    pub id: u32,
    pub name: String,
    pub description: String,
    pub repository_url: String,
    pub service_type: ServiceType,
    pub environments: Vec<ServiceEnvironment>,
}

// pub fn generate() -> Vec<Service> {
//     vec![
//         Service {
//             id: 1,
//             name: "Google".to_owned(),
//             description: "The searchiest searcher machine".to_owned(),
//             repository_url: "https://github.com".to_owned(),
//             service_type: ServiceType::WebApp,
//             environments: vec![
//                 ServiceEnvironment {
//                     id: 1,
//                     name: "Production".to_owned(),
//                     status: true,
//                     ping_url: "https://www.google.com".to_owned(),
//                     link_url: None,
//                 },
//                 ServiceEnvironment {
//                     id: 1,
//                     name: "Production also".to_owned(),
//                     status: false,
//                     ping_url: "https://www.google.com".to_owned(),
//                     link_url: None,
//                 },
//             ],
//         },
//         Service {
//             id: 1,
//             name: "Facebook".to_owned(),
//             description: "Isn't a \"book of faces\" kinda grotesque?".to_owned(),
//             repository_url: "https://developers.facebook.com/".to_owned(),
//             service_type: ServiceType::WebApp,
//             environments: vec![ServiceEnvironment {
//                 id: 1,
//                 name: "Production".to_owned(),
//                 status: false,
//                 ping_url: "https://www.facebook.com".to_owned(),
//                 link_url: None,
//             }],
//         },
//         Service {
//             id: 1,
//             name: "Ethan's Blog".to_owned(),
//             description: "My personal blog & website".to_owned(),
//             repository_url: "https://gitlab.com/freiguy1/freiguy1.gitlab.io/".to_owned(),
//             service_type: ServiceType::WebApp,
//             environments: vec![ServiceEnvironment {
//                 id: 1,
//                 name: "Production".to_owned(),
//                 status: false,
//                 ping_url: "https://ethanfrei.com".to_owned(),
//                 link_url: None,
//             }],
//         },
//         Service {
//             id: 1,
//             name: "Ethan's Notes".to_owned(),
//             description: "Repository of Ethan's notes".to_owned(),
//             repository_url: "https://www.gitlab.com/freiguy1/notes-public".to_owned(),
//             service_type: ServiceType::WebApp,
//             environments: vec![ServiceEnvironment {
//                 id: 1,
//                 name: "Production".to_owned(),
//                 status: false,
//                 ping_url: "https://notes.ethanfrei.com".to_owned(),
//                 link_url: None,
//             }],
//         },
//         Service {
//             id: 1,
//             name: "An API of Ice and Fire".to_owned(),
//             description: "API for data related to George R. R. Martin's book series".to_owned(),
//             repository_url: "https://www.gitlab.com".to_owned(),
//             service_type: ServiceType::DomainService,
//             environments: vec![
//                 ServiceEnvironment {
//                     id: 1,
//                     name: "Development".to_owned(),
//                     status: false,
//                     ping_url: "http://localhost:8080".to_owned(),
//                     link_url: None,
//                 },
//                 ServiceEnvironment {
//                     id: 1,
//                     name: "Production".to_owned(),
//                     status: false,
//                     ping_url: "https://anapioficeandfire.com/".to_owned(),
//                     link_url: None,
//                 },
//             ],
//         },
//     ]
// }
