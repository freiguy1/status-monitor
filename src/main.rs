use axum::{
    handler::Handler,
    response::sse::{Event, KeepAlive, Sse},
    routing::{delete, get, post},
    Extension, Router,
};
use futures::stream::Stream;
use serde::Serialize;
use std::convert::Infallible;
use std::sync::{Arc, Mutex};
use tera::Tera;
use tokio::sync::mpsc::{self, UnboundedSender};
use tokio_stream::wrappers::UnboundedReceiverStream;
use tokio_stream::StreamExt as _;

mod data;
mod handlers;
mod response;
mod tester;
mod util;

#[derive(Debug, Serialize)]
pub struct PushEvent {
    id: u32,
    html: String,
}

type Clients = Arc<Mutex<Vec<UnboundedSender<PushEvent>>>>;

#[tokio::main]
async fn main() {
    let clients: Clients = Arc::new(Mutex::new(Vec::new()));
    let repo = data::init().await.expect("Unable to initialize database");

    // if let Err(e) = data::init() {
    //     println!("Problem with sql initialization: {:?}", e);
    // }

    // pre-compile all templates
    let tera = match Tera::new("templates/**/*.html") {
        Ok(t) => Arc::new(t),
        Err(e) => {
            panic!("Parsing error(s): {}", e);
        }
    };

    let tester = tester::Tester::new(clients.clone(), tera.clone(), repo.clone());
    let _ = tokio::spawn(async move {
        tester.run().await;
    });

    let assets = axum_extra::routing::SpaRouter::new("/assets", "assets");
    let sse_router = Router::new()
        .route("/connect", get(user_connected))
        .layer(Extension(clients));
    let partials_router = Router::new()
        .route("/services", get(handlers::get_services_partial))
        .route(
            "/environment/create",
            get(handlers::get_environment_create_form_partial),
        )
        .route(
            "/service/:id/environments",
            get(handlers::get_environment_list_partial),
        );
    let app = Router::new()
        .route("/", get(handlers::get_root_page))
        .route("/about", get(handlers::get_about_page))
        .route(
            "/service/create",
            get(handlers::get_create_service_page).post(handlers::create_service),
        )
        .route("/service/:id/edit", get(handlers::get_edit_service_page))
        .route(
            "/service/:id",
            get(handlers::get_service_page)
                .put(handlers::edit_service)
                .delete(handlers::delete_service),
        )
        .route(
            "/service/:id/environment",
            post(handlers::create_environment),
        )
        .route(
            "/environment/:environment_id",
            delete(handlers::delete_environment),
        )
        .nest("/partials", partials_router)
        .layer(Extension(tera.clone()))
        .layer(Extension(repo))
        .merge(assets)
        .merge(sse_router)
        .fallback((move || async { handlers::not_found(tera) }).into_service());

    // run it with hyper on localhost:3000
    axum::Server::bind(&"0.0.0.0:3000".parse().unwrap())
        .serve(app.into_make_service())
        .await
        .unwrap();
}

async fn user_connected(
    Extension(clients): Extension<Clients>,
) -> Sse<impl Stream<Item = Result<Event, Infallible>>> {
    let (tx, rx) = mpsc::unbounded_channel();
    let rx = UnboundedReceiverStream::new(rx);

    // Save the sender in our list of connected users.
    clients.lock().unwrap().push(tx);

    // Convert messages into Server-Sent Events and return resulting stream.
    let stream = rx.map(|msg| {
        Ok(Event::default()
            .event(format!("{}_changed", msg.id))
            .data(msg.html))
    });

    Sse::new(stream).keep_alive(KeepAlive::default())
}
