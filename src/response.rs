use axum::http::StatusCode;
use axum::response::{Html, IntoResponse, Response as AxumResponse};
use sqlx::Error as SqlxError;
use std::sync::PoisonError;

pub type Response = std::result::Result<AxumResponse, ErrorResponse>;

pub fn html(content: String, no_cache: bool) -> Response {
    let mut response = Html(content).into_response();
    if no_cache {
        response.headers_mut().append(
            "Cache-Control",
            "no-store, must-revalidate".parse().unwrap(),
        );
    }
    Ok(response)
}

pub struct ErrorResponse(pub AxumResponse);

impl IntoResponse for ErrorResponse {
    fn into_response(self) -> AxumResponse {
        self.0
    }
}

impl From<SqlxError> for ErrorResponse {
    fn from(e: SqlxError) -> ErrorResponse {
        ErrorResponse((StatusCode::INTERNAL_SERVER_ERROR, e.to_string()).into_response())
    }
}

impl<T> From<PoisonError<T>> for ErrorResponse {
    fn from(e: PoisonError<T>) -> ErrorResponse {
        ErrorResponse((StatusCode::INTERNAL_SERVER_ERROR, e.to_string()).into_response())
    }
}

impl std::convert::From<tera::Error> for ErrorResponse {
    fn from(e: tera::Error) -> ErrorResponse {
        ErrorResponse((StatusCode::INTERNAL_SERVER_ERROR, e.to_string()).into_response())
    }
}
