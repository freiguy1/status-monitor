use super::Clients;
use crate::data::{Repository, Service};
use std::sync::Arc;
use tera::Tera;

type HttpClient =
    hyper::Client<hyper_tls::HttpsConnector<hyper::client::HttpConnector>, hyper::Body>;

pub struct Tester {
    repo: Repository,
    clients: Clients,
    tera: Arc<Tera>,
    http_client: HttpClient,
}

impl Tester {
    pub fn new(clients: Clients, tera: Arc<Tera>, repo: Repository) -> Self {
        let https = hyper_tls::HttpsConnector::new();
        let http_client = hyper::Client::builder().build::<_, hyper::Body>(https);
        Self {
            clients,
            tera,
            http_client,
            repo,
        }
    }

    pub async fn run(&self) {
        let mut previous_services: Vec<Service> = vec![];

        // For testing
        tokio::time::sleep(std::time::Duration::from_secs(3)).await;

        loop {
            let mut services = match self.repo.get_all_services().await {
                Ok(s) => s,
                Err(e) => {
                    println!(
                        "Unable to get services while testing all endpoints. {:?}",
                        e
                    );
                    Self::sleep_between_tests().await;
                    continue;
                }
            };

            for mut service in services.iter_mut() {
                let previous_service = previous_services.iter().find(|s| s.id == service.id);
                let service_changed = match self.test_service(&mut service, previous_service).await
                {
                    Ok(sc) => sc,
                    Err(e) => {
                        println!("SQL error happened when testing service. {:?}", e);
                        continue;
                    }
                };
                if service_changed {
                    if let Err(_) = self.push_update_message(&service) {
                        println!("Some RwLock based error happened when pushing update message.");
                    }
                }
            }

            previous_services = services.clone();
            Self::sleep_between_tests().await;
        }
    }

    async fn sleep_between_tests() {
        tokio::time::sleep(std::time::Duration::from_secs(5)).await;
    }

    fn push_update_message(&self, service: &Service) -> Result<(), ()> {
        let mut context = tera::Context::new();
        context.insert("service", service);
        let html = self
            .tera
            .render("partials/service_list_item.html", &context)
            .unwrap(); // leaving unwrap because problematic templates should crash app

        self.clients.lock().unwrap().retain(|c| {
            c.send(super::PushEvent {
                id: service.id,
                html: html.clone(),
            })
            .is_ok()
        });
        Ok(())
    }

    async fn test_service(
        &self,
        service: &mut Service,
        previous_service: Option<&Service>,
    ) -> Result<bool, sqlx::Error> {
        let mut service_changed = previous_service.map(|ps| ps != service).unwrap_or(false);
        for env in service.environments.iter_mut() {
            // for testing: let new_status = !env.status;
            let new_status = self.test_endpoint(&env.ping_url.clone()).await;
            if new_status != env.status {
                self.repo.update_env_status(env.id, new_status).await?;
                env.status = new_status;
                service_changed = true;
            }
        }
        Ok(service_changed)
    }

    async fn test_endpoint(&self, uri: &String) -> bool {
        let uri_struct = match hyper::Uri::try_from(uri) {
            Ok(u) => u,
            Err(_) => return false,
        };
        let response = self.http_client.get(uri_struct).await;
        if let Err(e) = response.as_ref() {
            println!("Unsuccessful test_endpoint for {}. error: {:?}", uri, e);
        }

        response.is_ok()
    }
}
