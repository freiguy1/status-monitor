use crate::data::Repository;
use crate::response::{html, Response};
use crate::util::url_with_protocol;
use axum::extract::{Path, Query};
use axum::http::StatusCode;
use axum::response::IntoResponse;
use axum::{Extension, Form};
use serde::Deserialize;
use std::collections::HashMap;
use std::sync::Arc;
use tera::Tera;

pub async fn get_root_page(
    Extension(tera): Extension<Arc<Tera>>,
    Extension(repo): Extension<Repository>,
    Query(params): Query<HashMap<String, String>>,
) -> Response {
    let services = repo.get_all_services().await?;
    // TODO: Extract search logic, or put in sql query
    let filtered = if let Some(name_filter) = params.get("q") {
        let name_filter_lower = name_filter.to_lowercase();
        services
            .into_iter()
            .filter(|s| s.name.to_lowercase().contains(&name_filter_lower))
            .collect()
    } else {
        services
    };

    let mut context = tera::Context::new();
    context.insert("services", &*filtered);
    context.insert("q", params.get("q").unwrap_or(&"".to_string()));
    let rendered = tera.render("index.html", &context)?;
    return html(rendered, true);
}

pub async fn get_about_page(Extension(tera): Extension<Arc<Tera>>) -> Response {
    let context = tera::Context::new();
    let rendered = tera.render("about.html", &context)?;
    html(rendered, false)
}

pub async fn get_service_page(
    Path(id): Path<u32>,
    Extension(tera): Extension<Arc<Tera>>,
    Extension(repo): Extension<Repository>,
) -> Response {
    let context = match repo
        .get_service(id)
        .await?
        .and_then(|s| tera::Context::from_serialize(s).ok())
    {
        Some(s) => s,
        None => return Ok(not_found(tera).into_response()),
    };
    let rendered = tera.render("service.html", &context)?;
    html(rendered, true)
}

pub async fn get_edit_service_page(
    Path(id): Path<u32>,
    Extension(tera): Extension<Arc<Tera>>,
    Extension(repo): Extension<Repository>,
) -> Response {
    let service = match repo.get_service(id).await? {
        Some(s) => s,
        None => return Ok(not_found(tera).into_response()),
    };
    let mut context = tera::Context::from_serialize(&service).expect("Unable to serialize service");
    context.insert("type_id", &service.service_type.to_u32());
    let rendered = tera.render("create_edit_service.html", &context)?;
    html(rendered, false)
}

pub async fn get_create_service_page(Extension(tera): Extension<Arc<Tera>>) -> Response {
    let context = tera::Context::new();
    let rendered = tera.render("create_edit_service.html", &context)?;
    html(rendered, false)
}

#[derive(Deserialize)]
pub struct CreateEditServiceForm {
    name: String,
    #[serde(rename = "type")]
    service_type: String,
    description: String,
    repository_url: String,
}

pub async fn create_service(
    Form(input): Form<CreateEditServiceForm>,
    Extension(repo): Extension<Repository>,
) -> Response {
    let parsed_service_type = match input.service_type.parse::<u32>().map(|num| num.into()) {
        Ok(st) => st,
        Err(_) => return Ok((StatusCode::BAD_REQUEST, "Invalid service_type").into_response()),
    };

    let new_service = repo
        .create_service(
            &input.name,
            &input.description,
            &input.repository_url,
            parsed_service_type,
        )
        .await?;

    let mut response = StatusCode::CREATED.into_response();
    response.headers_mut().append(
        "HX-Redirect",
        format!("/service/{}", new_service.id).parse().unwrap(),
    );

    Ok(response)
}

pub async fn edit_service(
    Path(id): Path<u32>,
    Form(input): Form<CreateEditServiceForm>,
    Extension(repo): Extension<Repository>,
) -> Response {
    let parsed_service_type = match input.service_type.parse::<u32>().map(|num| num.into()) {
        Ok(st) => st,
        Err(_) => return Ok((StatusCode::BAD_REQUEST, "Invalid service_type").into_response()),
    };

    repo.edit_service(
        id,
        &input.name,
        &input.description,
        &input.repository_url,
        parsed_service_type,
    )
    .await?;

    let mut response = StatusCode::NO_CONTENT.into_response();
    response
        .headers_mut()
        .append("HX-Redirect", format!("/service/{}", id).parse().unwrap());

    Ok(response)
}

pub async fn delete_service(
    Path(id): Path<u32>,
    Extension(repo): Extension<Repository>,
) -> Response {
    repo.delete_service(id).await?;

    let mut response = StatusCode::NO_CONTENT.into_response();
    response
        .headers_mut()
        .append("HX-Redirect", "/".parse().unwrap());

    Ok(response)
}

pub async fn get_services_partial(
    Extension(tera): Extension<Arc<Tera>>,
    Extension(repo): Extension<Repository>,
    Query(params): Query<HashMap<String, String>>,
) -> Response {
    let services = repo.get_all_services().await?;

    let (filtered, new_url) = if let Some(name_filter) = params.get("q") {
        if name_filter.is_empty() {
            (services, "/".to_string())
        } else {
            let name_filter_lower = name_filter.to_lowercase();
            let f = services
                .into_iter()
                .filter(|s| s.name.to_lowercase().contains(&name_filter_lower))
                .collect();
            (f, format!("/?q={}", name_filter))
        }
    } else {
        (services, "/".to_string())
    };

    let mut context = tera::Context::new();
    context.insert("services", &*filtered);
    let rendered = tera.render("partials/services.html", &context)?;
    let mut response = axum::response::Html(rendered).into_response();
    response
        .headers_mut()
        .append("HX-Replace-Url", new_url.parse().unwrap());
    Ok(response)
}

pub async fn get_environment_create_form_partial(
    Extension(tera): Extension<Arc<Tera>>,
) -> Response {
    let context = tera::Context::new();
    let rendered = tera.render("partials/environment_form.html", &context)?;
    html(rendered, false)
}

#[derive(Deserialize)]
pub struct CreateEnvironmentForm {
    name: String,
    ping_url: String,
    link_url: String,
}

pub async fn create_environment(
    Path(service_id): Path<u32>,
    Form(input): Form<CreateEnvironmentForm>,
    Extension(tera): Extension<Arc<Tera>>,
    Extension(repo): Extension<Repository>,
) -> Response {
    // Some data cleaning
    let ping_url = url_with_protocol(&input.ping_url);
    let link_url: Option<String> = if input.link_url.is_empty() {
        None
    } else {
        Some(url_with_protocol(&input.link_url))
    };

    repo.create_environment(service_id, &input.name, &ping_url, &link_url)
        .await?;

    let updated_service = repo.get_service(service_id).await?.unwrap();

    let context =
        tera::Context::from_serialize(updated_service).expect("Unable to serialize new service");
    let rendered = tera.render("partials/service_content.html", &context)?;
    html(rendered, false)
}

pub async fn delete_environment(
    Path(environment_id): Path<u32>,
    Extension(tera): Extension<Arc<Tera>>,
    Extension(repo): Extension<Repository>,
) -> Response {
    let env = match repo.get_environment(environment_id).await? {
        Some(e) => e,
        None => return Ok(not_found(tera).into_response()),
    };
    repo.delete_environment(environment_id).await?;

    let environments = repo.get_environments_by_service_id(env.service_id).await?;
    let mut context = tera::Context::new();
    context.insert("environments", &environments);
    let rendered = tera.render("partials/environment_list.html", &context)?;
    html(rendered, false)
}

pub async fn get_environment_list_partial(
    Path(id): Path<u32>,
    Extension(tera): Extension<Arc<Tera>>,
    Extension(repo): Extension<Repository>,
) -> Response {
    let environments = repo.get_environments_by_service_id(id).await?;
    let mut context = tera::Context::new();
    context.insert("environments", &environments);
    let rendered = tera.render("partials/environment_list.html", &context)?;
    html(rendered, false)
}

pub fn not_found(tera: Arc<Tera>) -> impl axum::response::IntoResponse {
    let rendered = tera
        .render("not_found.html", &tera::Context::new())
        .unwrap_or_else(|e| format!("Something didn't work.\n{}", e));
    (
        axum::http::StatusCode::NOT_FOUND,
        axum::response::Html(rendered),
    )
}
