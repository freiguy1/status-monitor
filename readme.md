# About

This project is an educational project for learning server-sent events, htmx, more rust, and experimenting with a no-node/npm/webpack/SPA/Sass design. [deployed instance](https://status-monitor.fly.dev)

# Motivation

My professional development experience involves lots of javascript. It involves a complex (and mysterious) process for building the app and bundling for production. It has critical vulnerabilites in dependencies that are hard to resolve. It has constantly outdated packages which aren't trivial to update. It has `any` being used in typescript which puts me back in javascript land.

I've been wanting to try something a bit simpler. Somewhere I heard about [htmx](http://htmx.org) which seems like a pretty cool spin on the SPA mentality. My goal is to use very minimal handwritten javascript (can it be done with none?), but maintain the speed and user experience people enjoy when using traditional SPAs.

While rust is certainly not sticking with the _simplistic_ theme (it's a pretty complex language, and I'm still using hundreds of MB of dependencies), it is a pretty hot topic these days, and my current familiarity means I can keep some velocity on that part of the app. I guess one thing that is pretty minimal about rust is the resulting binary and docker image. With an Alpine base image, its currently only 23MB!

# Running

Create an empty dir in the root of the project named `data/`. Eventually the code could make it if it doesn't exist, but I'm too lazy.
Simply `cargo run` to host it on your local machine. `cargo watch -x run` to rebuild on each file change (requires `cargo install cargo-watch`). That's helpful when just changing templates quickly.

# Docker

Docker's pretty straightforward: `docker build -t status-monitor .` then `docker run -p 3000:3000 status-monitor`.

# Deployment

At this point, I'm using fly.io for the first time, and it's pretty nice and easy. [Install](https://fly.io/docs/getting-started/installing-flyctl/) `flyctl` then run `fly deploy` once authenticated.

This fly app requires a persistant data volume named "database". For this, use [fly.io's volumes](https://fly.io/docs/reference/volumes/).

Create volume:
- `fly volumes create database -s 1 -r ord`

Delete volume:
- Get volume id with `fly volumes list`
- Delete with `fly volumes delete <id>`

# Todo!

- [x] about page
- [x] edit service
- [x] delete environment
- [x] delete service
- [x] light/dark mode
- [x] address incomplete URLs (lacking protocol)
- [x] more data-intensive index page
- [x] search on index page
- [ ] sorting/ordering (?)

# Notes:
### Chota Screen Breakpoints:
- sm: 600px
- md: 900px
- lg: 1200px

### Downsides of no js
- Immediately reactive form validation tricky
